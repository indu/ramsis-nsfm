from datetime import datetime

import numpy as np
from etas.inversion import ETASParameterCalculation
from etas.simulation import ETASSimulation
from obspy import Catalog
from obspy.io.quakeml.core import Pickler
from ramsis.datamodel import ResultTimeBin
from ramsis.io.seismics import QuakeMLForecastCatalogDeserializer
from ramsis.utils.io.catalogs import df_to_catalog

from ramsis_nsfm.models import NaturalSFM


class ETASCalculation(NaturalSFM):

    def __init__(self, *args, **kwargs):
        self._logger.info('Initialize')

        self.__seismicity = self.transform_seismicity()
        self.__shape_coordinates = self.transform_geometry()

        self.parameters = None
        self.simulation = None

    def transform_geometry(self):
        coordinates = self._geometry.exterior.coords
        geometry_array = np.array(list(coordinates))
        return geometry_array

    def transform_seismicity(self):
        select_cols = {'index': 'time',
                       'mag': 'magnitude',
                       'Lat': 'latitude',
                       'Lon': 'longitude'}

        seismicity = self._seismicity.reset_index().rename(
            columns=select_cols)[select_cols.values()]

        seismicity['mc_current'] = np.where(
            seismicity.time < datetime(
                1992, 1, 1), 2.7, 2.3)
        return seismicity

    def fit(self):
        input_dict = {
            'catalog': self.__seismicity,
            'auxiliary_start': self._model_parameters['auxiliary_start'],
            'timewindow_start': self._model_parameters['timewindow_start'],
            'timewindow_end': self._forecast_start,
            'theta_0': self._model_parameters['theta_0'],
            'mc': self._model_parameters['mc'],
            'delta_m': self._model_parameters['delta_m'],
            'coppersmith_multiplier':
            self._model_parameters['coppersmith_multiplier'],
            'earth_radius': self._model_parameters['earth_radius'],
            'shape_coords': self.__shape_coordinates}

        self.parameters = ETASParameterCalculation(input_dict)
        self.parameters.prepare()
        self.parameters.invert()

    def predict(self):
        self.simulation = ETASSimulation(self.parameters)
        self.simulation.prepare()
        forecast_duration = self._forecast_end - self._forecast_start
        return self.simulation.simulate_to_df(
            forecast_duration.days, self._model_parameters['n_simulations'])

    def parse_results(self, results):
        parsed = results.rename(
            columns={'latitude': 'Ys',
                     'longitude': 'Xs',
                     'magnitude': 'mag'})
        parsed.set_index('time', inplace=True)
        parsed['Zs'] = 0

        deserializer = QuakeMLForecastCatalogDeserializer()

        # extract catalogs
        grouped = parsed.groupby('catalog_id')

        time_result = ResultTimeBin(
            starttime=self._forecast_start,
            endtime=self._forecast_end,
            seismicforecastcatalogs=[])

        for id, group in grouped:
            empty_catalog = df_to_catalog(group, self._proj_string)
            nsmap_ = getattr(empty_catalog, "nsmap", {})
            xml_doc = Pickler(nsmap=nsmap_).dumps(empty_catalog)
            forecast_catalog = deserializer.loads(xml_doc)
            time_result.seismicforecastcatalogs.append(forecast_catalog)

        # rest of catalogs are empty, append them too
        empty_catalog = Catalog()
        nsmap_ = getattr(empty_catalog, "nsmap", {})
        xml_doc = Pickler(nsmap=nsmap_).dumps(empty_catalog)

        for _ in range(0, self._model_parameters['n_simulations']
                       - grouped.ngroups):
            time_result.seismicforecastcatalogs.append(
                deserializer.loads(xml_doc))

        return [time_result]

    def run(self):
        self.fit()
        results = self.predict()
        forecast = self.parse_results(results)

        return forecast

    def run_raw(self):
        self.fit()
        results = self.predict()
        return results
