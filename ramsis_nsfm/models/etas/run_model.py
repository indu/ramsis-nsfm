import logging
from datetime import datetime, timedelta

import numpy as np
from obspy import read_events  # noqa
from obspy.clients.fdsn import Client  # noqa
from ramsis.datamodel import InputParameters, ModelConfig
from ramsis.sfm.schemas import SFMInput
from ramsis.utils.io.coordinates import bounding_box_to_polygon


from ramsis_nsfm.models.etas import ETASCalculation

date_strftime_format = "%d-%m-%y %H:%M:%S"
logging.basicConfig(
    format='%(asctime)s : %(levelname)-8s : %(name)-15s - %(message)s',
    datefmt=date_strftime_format,
    level=logging.INFO)

format = '%Y-%m-%d %H:%M:%S'
auxiliary_start = datetime.strptime("1992-01-01 00:00:00", format)
timewindow_start = datetime.strptime("1997-01-01 00:00:00", format)
timewindow_end = datetime.strptime("2022-01-01 00:00:00", format)

# client = Client('http://arclink.ethz.ch', timeout=300)
# catalog = client.get_events(
#     starttime=auxiliary_start,
#     endtime=timewindow_end,
#     minmagnitude=2.3
# )
catalog = read_events('input_data/catalog.xml', format='QUAKEML')

shape = np.array([[45.7, 47.9, 47.9, 45.7, 45.7],
                 [5.85, 5.85, 10.6, 10.6, 5.85],
                 [0, 0, 0, 0, 0]])


geometry_extent = bounding_box_to_polygon(
    min_x=shape[0].min(), max_x=shape[0].max(),
    min_y=shape[1].min(), max_y=shape[1].max())


forecast_duration = 30 * 24 * 60 * 60  # seconds

data = SFMInput(
    forecast_start=timewindow_end,
    forecast_end=timewindow_end + timedelta(seconds=forecast_duration),
    geometry_extent=geometry_extent,
    seismic_catalog=catalog,
    model_config=ModelConfig(
        InputParameters(
            model_parameters={
                "theta_0": {
                    "log10_mu": -6.21,
                    "log10_k0": -2.75,
                    "a": 1.13,
                    "log10_c": -2.85,
                    "omega": -0.13,
                    "log10_tau": 3.57,
                    "log10_d": -0.51,
                    "gamma": 0.15,
                    "rho": 0.63
                },
                "mc": 2.3,
                "delta_m": 0.1,
                "coppersmith_multiplier": 100,
                "earth_radius": 6.3781e3,
                "auxiliary_start": "1992-01-01 00:00:00",
                "timewindow_start": "1997-01-01 00:00:00",
                "n_simulations": 100
            },
            wrapper_parameters={
                "epoch_duration": forecast_duration,  # in seconds
            }
        )
    )
)

myobj = ETASCalculation.initialize(data)

print(myobj.seismicity)

# result = myobj.run()
# print(result)
# print(result.subgeometries)
