from io import BytesIO
from typing import Union

from obspy import Catalog
from ramsis.sfm.schemas import SFMInput
from ramsis.sfm.wrapper import SFM
from ramsis.utils.io import catalogs


class NaturalSFM(SFM):

    @classmethod
    def initialize(cls, input: SFMInput, *args, **kwargs):

        obj = cls.__new__(cls, *args, from_method=True, **kwargs)

        obj._model_parameters = input.model_config.config.model_parameters
        obj._wrapper_parameters = input.model_config.config.wrapper_parameters
        # Always expect data to come in this projection.
        obj._proj_string = 'EPSG:4326'
        obj._forecast_start = input.forecast_start
        obj._forecast_end = input.forecast_end
        # This is now a polygon.
        obj._geometry = input.geometry_extent
        # Do we need to set this to the duration of the forecast?
        # obj._epoch_duration = input.epoch_duration

        obj.seismicity = input.seismic_catalog

        obj.__init__(*args, **kwargs)

        return obj

    @property
    def seismicity(self):
        return catalogs.df_to_catalog(self._seismicity, self._proj_string)

    @seismicity.setter
    def seismicity(self, data: Union[Catalog, BytesIO]) -> None:
        self._seismicity = catalogs.catalog_to_df(
            data, self._proj_string)
